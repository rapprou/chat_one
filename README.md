# Javascript FS
Chat simple avec javascript, pour le frontend + backend et la base de données en Mongo. 

# Technologies utilisées
- Nodejs
  - Express
  - Socket.io
- Mongodb

# Liens et ressources utilisés dans ce projet
- [Bootstrap4 CDN](http://getbootstrap.com/docs/4.0/getting-started/introduction/)
- [Background Gradient Color](https://uigradients.com/#Lawrencium)
- [jQuery CDN](https://code.jquery.com/)
- [Mongodb](https://www.mongodb.com/fr-fr)

# En ligne
- [chat](https://chat-beweb.herokuapp.com/)

