import { Schema, model } from "mongoose";

//string pour appeler depuis la base donées mongo schema chat
const ChatSchema = new Schema({
  nick: String,
  msg: String,
  created: { type: Date, default: Date.now },
});

export default model("Chat", ChatSchema);
